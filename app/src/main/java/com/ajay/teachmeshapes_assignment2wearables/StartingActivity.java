package com.ajay.teachmeshapes_assignment2wearables;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEvent;
import io.particle.android.sdk.cloud.ParticleEventHandler;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class StartingActivity extends AppCompatActivity {

    private final String TAG="TEACH_ME_SHAPES";

    // Particle Account Info
    private final String PARTICLE_USERNAME = "letv5050@gmail.com";
    private final String PARTICLE_PASSWORD = "passion100";

    // Particle device-specific info
    private final String DEVICE_ID = "38002a000247363333343435";

    // Particle Publish / Subscribe variables
    private long subscriptionId;

    // Particle device
    private ParticleDevice myDevice;

    Button btnRandom;
    Button btnActivity1;
    Button btnActivity2;

    Random rnd;
    int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting);

        this.btnActivity1 = findViewById(R.id.btnActivity1);
        this.btnActivity2 = findViewById(R.id.btnActivity2);
        this.btnRandom = findViewById(R.id.btnRandom);

        this.rnd = new Random();

        // Initialize the connection to Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // Setup your device variable
        getDeviceFromCloud();
    }

    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                myDevice = particleCloud.getDevice(DEVICE_ID);

                //Set the Activity Number on Particle to 0.
                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add("0");
                try {
                    myDevice.callFunction("setActivityNumber", functionParameters);
                    Log.d(TAG, "Successfully set Activity Number to 0");
                }
                catch (ParticleDevice.FunctionDoesNotExistException e1) {
                    e1.printStackTrace();
                }

                return -1;

            }

            @Override
            public void onSuccess(Object o) {

                Log.d(TAG, "Successfully got device from Cloud");

                // if you get the device, then go subscribe to events
                subscribeToParticleEvents();
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    public void startAct1(){
        Intent myIntent = new Intent(getBaseContext(),   MainActivity.class);
        startActivity(myIntent);
    }
    public void startAct2(){
        Intent myIntent = new Intent(getBaseContext(),   Activity2.class);
        startActivity(myIntent);
    }
    public void startRandomActivity(){
        this.i = rnd.nextInt(2);
        if (this.i == 0){
            Intent myIntent = new Intent(getBaseContext(),   MainActivity.class);
            startActivity(myIntent);
        }
        else{
            Intent myIntent = new Intent(getBaseContext(),   Activity2.class);
            startActivity(myIntent);
        }
    }

    public void goToActivity1(View view){
        this.startAct1();
    }

    public void goToActivity2(View view){
        this.startAct2();
    }

    public void goToRandomActivity(View view){
        this.startRandomActivity();
    }

    public void subscribeToParticleEvents() {
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                subscriptionId = ParticleCloudSDK.getCloud().subscribeToAllEvents(
                        "selectedOption",  // the first argument, "eventNamePrefix", is optional
                        new ParticleEventHandler() {
                            public void onEvent(String eventName, ParticleEvent event) {
                                Log.i(TAG, "Received event with payload: " + event.dataPayload);
                                String choice = event.dataPayload;


                                if (choice.contentEquals("activity1")) {
                                    startAct1();
                                    }
                                if (choice.contentEquals("activity2")) {
                                    startAct2();
                                }
                                if (choice.contentEquals("randomActivity")) {
                                    startRandomActivity();
                                }

                            }

                            public void onEventError(Exception e) {
                                Log.e(TAG, "Event error: ", e);
                            }
                        });


                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Success: Subscribed to events!");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }
}
