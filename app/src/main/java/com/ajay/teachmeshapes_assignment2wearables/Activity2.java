package com.ajay.teachmeshapes_assignment2wearables;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEvent;
import io.particle.android.sdk.cloud.ParticleEventHandler;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class Activity2 extends AppCompatActivity {

    private final String TAG="TEACH_ME_SHAPES";

    // Particle Account Info
    private final String PARTICLE_USERNAME = "letv5050@gmail.com";
    private final String PARTICLE_PASSWORD = "passion100";

    // Particle device-specific info
    private final String DEVICE_ID = "38002a000247363333343435";

    // Particle Publish / Subscribe variables
    private long subscriptionId;

    // Particle device
    private ParticleDevice myDevice;

    ImageView imgRotShape;
    Button btnRotate;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        this.imgRotShape = findViewById(R.id.imgRotShape);
        this.btnRotate = findViewById(R.id.btnRotate);
        this.count = 0;

        // Initialize the connection to Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // Setup your device variable
        getDeviceFromCloud();
    }

    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                myDevice = particleCloud.getDevice(DEVICE_ID);

                //Set the Activity Number on Particle to 2.
                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add("2");
                try {
                    myDevice.callFunction("setActivityNumber", functionParameters);
                    Log.d(TAG, "Successfully set Activity Number to 2");
                }
                catch (ParticleDevice.FunctionDoesNotExistException e1) {
                    e1.printStackTrace();
                }

                return -1;

            }

            @Override
            public void onSuccess(Object o) {

                Log.d(TAG, "Successfully got device from Cloud");

                // if you get the device, then go subscribe to events
                subscribeToParticleEvents();
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    public void rotateShapeFromParticle() {

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {

                try {
                    double rotationAngle = myDevice.getDoubleVariable("angle");
                    imgRotShape.setRotation((float) rotationAngle);
                    System.out.println("Rotation Angle:" +rotationAngle);

                    rotateShapeFromParticle();

                } catch (ParticleDevice.VariableDoesNotExistException e) {
                    e.printStackTrace();
                }


                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                // put your success message here
                Log.d(TAG, "Success: Rotated!");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                // put your error handling code here
                Log.d(TAG, exception.getBestMessage());
            }
        });

    }




    public void startActivity1(View view){
        Intent myIntent = new Intent(getBaseContext(),   MainActivity.class);
        startActivity(myIntent);
    }

    public void gotoStartingActivity(View view){
        Intent myIntent = new Intent(getBaseContext(),   StartingActivity.class);
        startActivity(myIntent);
    }



    public void rotateImage(View view){
        this.rotateShapeFromParticle();
        //imgRotShape.setRotation(count);
        //this.count = this.count + 10;
    }

    public void subscribeToParticleEvents() {
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                subscriptionId = ParticleCloudSDK.getCloud().subscribeToAllEvents(
                        "selectedOption",  // the first argument, "eventNamePrefix", is optional
                        new ParticleEventHandler() {
                            public void onEvent(String eventName, ParticleEvent event) {
                                Log.i(TAG, "Received event with payload: " + event.dataPayload);
                                String choice = event.dataPayload;


                                if (choice.contentEquals("next")) {
                                    imgRotShape.setImageResource(R.drawable.triangle);
                                }

                                if (choice.contentEquals("prev")) {
                                    imgRotShape.setImageResource(R.drawable.hexagon);
                                }

                                if (choice.contentEquals("rotate")) {
                                    rotateShapeFromParticle();
                                }

                                if (choice.contentEquals("startingActivity")) {
                                    Intent myIntent = new Intent(getBaseContext(),   StartingActivity.class);
                                    startActivity(myIntent);
                                }


                            }

                            public void onEventError(Exception e) {
                                Log.e(TAG, "Event error: ", e);
                            }
                        });


                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Success: Subscribed to events!");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }
}
