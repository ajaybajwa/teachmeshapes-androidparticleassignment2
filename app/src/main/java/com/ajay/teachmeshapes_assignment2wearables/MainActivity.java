package com.ajay.teachmeshapes_assignment2wearables;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEvent;
import io.particle.android.sdk.cloud.ParticleEventHandler;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity {

    private final String TAG="TEACH_ME_SHAPES";

    // Particle Account Info
    private final String PARTICLE_USERNAME = "letv5050@gmail.com";
    private final String PARTICLE_PASSWORD = "passion100";

    // Particle device-specific info
    private final String DEVICE_ID = "38002a000247363333343435";

    // Particle Publish / Subscribe variables
    private long subscriptionId;

    // Particle device
    private ParticleDevice myDevice;

    TextView txtAnswerResult;
    TextView txtQuestion;
    ImageView imgShape;
    TextView txtScore;

    int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         this.txtAnswerResult = findViewById(R.id.txtAnswerResult);
         this.imgShape = findViewById(R.id.imgShape);
         this.txtQuestion = findViewById(R.id.txtQuestion);
         this.txtScore = findViewById(R.id.txtScore);

        // Initialize the connection to Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // Setup your device variable
        getDeviceFromCloud();
        //setActivtyNumber();

    }


    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                myDevice = particleCloud.getDevice(DEVICE_ID);

                //Set the Activity Number on Particle to 1.
                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add("1");
                try {
                    myDevice.callFunction("setActivityNumber", functionParameters);
                    Log.d(TAG, "Successfully set Activity Number to 1");
                }
                catch (ParticleDevice.FunctionDoesNotExistException e1) {
                    e1.printStackTrace();
                }

                return -1;

            }

            @Override
            public void onSuccess(Object o) {

                Log.d(TAG, "Successfully got device from Cloud");

                // if you get the device, then go subscribe to events
                subscribeToParticleEvents();
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }


    public void indicateCorrectAnswer() {

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                // put your logic here to talk to the particle
                // --------------------------------------------
                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add("green");
                try {
                    myDevice.callFunction("showAnswerResult", functionParameters);

                } catch (ParticleDevice.FunctionDoesNotExistException e1) {
                    e1.printStackTrace();
                }


                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                // put your success message here
                Log.d(TAG, "Success: Turned light green!!");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                // put your error handling code here
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    public void indicateIncorrectAnswer() {

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                // put your logic here to talk to the particle
                // --------------------------------------------
                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add("red");
                try {
                    myDevice.callFunction("showAnswerResult", functionParameters);

                } catch (ParticleDevice.FunctionDoesNotExistException e1) {
                    e1.printStackTrace();
                }


                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                // put your success message here
                Log.d(TAG, "Success: Turned lights red!!");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                // put your error handling code here
                Log.d(TAG, exception.getBestMessage());
            }
        });

    }

    public void showShape1(){
        this.imgShape.setImageResource(R.drawable.hexagon);
        this.txtQuestion.setText("How many sides does this shape have?\nA. 8\nB. 6\n\nUse Particle Buttons to Select the Answer.\n     Button 1 for Option A.\n     Button 3 for Option B.");
    }

    public void showShape2(){
        this.imgShape.setImageResource(0);
        this.imgShape.setImageResource(R.drawable.triangle);
        this.txtQuestion.setText("How many sides does this shape have?\nA. 3\nB. 5\n\nUse Particle Buttons to Select the Answer.\n     Button 1 for Option A.\n     Button 3 for Option B.");
    }



    public void subscribeToParticleEvents() {
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                subscriptionId = ParticleCloudSDK.getCloud().subscribeToAllEvents(
                        "selectedOption",  // the first argument, "eventNamePrefix", is optional
                        new ParticleEventHandler() {
                            public void onEvent(String eventName, ParticleEvent event) {
                                Log.i(TAG, "Received event with payload: " + event.dataPayload);
                                String choice = event.dataPayload;

                                //If no answer is selected, only then increase the score
                                if (txtAnswerResult.getCurrentTextColor() == Color.BLACK){

                                    if (choice.contentEquals("B")) {
                                        if (imgShape.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.hexagon,getTheme()).getConstantState()){
                                            System.out.println("SHAPE IS HEXAGON");
                                            indicateCorrectAnswer();
                                            txtAnswerResult.setVisibility(View.VISIBLE);
                                            txtAnswerResult.setTextColor(Color.GREEN);
                                            txtAnswerResult.setText("Correct Answer\nPress & Hold Button 4 for next Shape.\nPress Button 2 for Score.");
                                            score = score + 1;
                                        }
                                        else{
                                            System.out.println("SHAPE IS TRIANGLE");
                                            indicateIncorrectAnswer();
                                            txtAnswerResult.setVisibility(View.VISIBLE);
                                            txtAnswerResult.setTextColor(Color.RED);
                                            txtAnswerResult.setText("Incorrect Answer\nPress & Hold Button 2 for previous Shape.\nPress Button 2 for Score.");
                                        }


                                    }
                                    else if (choice.contentEquals("A")) {
                                        if (imgShape.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.hexagon,getTheme()).getConstantState()){
                                            System.out.println("SHAPE IS HEXAGON");
                                            indicateIncorrectAnswer();
                                            txtAnswerResult.setVisibility(View.VISIBLE);
                                            txtAnswerResult.setTextColor(Color.RED);
                                            txtAnswerResult.setText("Incorrect Answer\nPress & Hold Button 4 for next Shape.\nPress Button 2 for Score.");
                                        }
                                        else{
                                            indicateCorrectAnswer();
                                            txtAnswerResult.setVisibility(View.VISIBLE);
                                            txtAnswerResult.setTextColor(Color.GREEN);
                                            txtAnswerResult.setText("Correct Answer\nPress & Hold Button 2 for previous Shape.\nPress Button 2 for Score.");
                                            score = score + 1;
                                        }

                                    }
                                }

                                if (choice.contentEquals("next")) {
                                    showShape2();
                                    txtAnswerResult.setTextColor(Color.BLACK);
                                    txtAnswerResult.setText(null);
                                }
                                if (choice.contentEquals("prev")) {
                                    showShape1();
                                    txtAnswerResult.setTextColor(Color.BLACK);
                                    txtAnswerResult.setText(null);
                                }
                                if (choice.contentEquals("showScore")){
                                    try{
                                        txtScore.setText("Score: "+myDevice.getIntVariable("scoreCount"));
                                    }
                                    catch (Exception e){
                                        System.out.println("Error getting Score");
                                    }

                                }

                                System.out.println("Score: "+score);
                            }

                            public void onEventError(Exception e) {
                                Log.e(TAG, "Event error: ", e);
                            }
                        });


                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Success: Subscribed to events!");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }
    public void startActivity2(View view){
        Intent myIntent = new Intent(getBaseContext(),   Activity2.class);
        startActivity(myIntent);
    }

    public void gotoStartingActivity(View view){
        Intent myIntent = new Intent(getBaseContext(),   StartingActivity.class);
        startActivity(myIntent);
    }
}
